import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-form-comp',
  templateUrl: './form-comp.component.html',
  styleUrls: ['./form-comp.component.scss']
})
export class FormCompComponent implements OnInit {
  @Input('testForm') testForm: FormGroup;
  @Input('formDetails') formDetails: any;
  @Output('submitForm') submitForm = new EventEmitter<any>()
  constructor() { 
  }

  ngOnInit() {
  }
  onSubmit(value) {
    this.submitForm.emit(value);
  }

}
