import { Component, OnInit } from '@angular/core';
import { FormDetails } from '../models/form.model';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  formDetails = FormDetails.Login; 
  constructor(private fb: FormBuilder) { 
    this.loginForm = this.fb.group({});
  }

  ngOnInit() {
    this.createForm();
    console.log(this.loginForm);
  }

  createForm() {
    this.formDetails.forEach(detail => {
      this.loginForm.addControl(detail.property, new FormControl(''));
    })
  }

  onSubmit(e) {
    console.log(e);
  }

}
