import { Component, OnInit } from '@angular/core';
import { FormDetails } from '../models/form.model';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  registerForm:FormGroup;
  formDetails = FormDetails.Register; 
  constructor(private fb: FormBuilder) { 
    this.registerForm = this.fb.group({});
  }

  ngOnInit() {
    this.createForm();
    console.log(this.registerForm);
  }

  createForm() {
    this.formDetails.forEach(detail => {
      this.registerForm.addControl(detail.property, new FormControl(''));
    })
  }

  onSubmit(e) {
    console.log(e);
  }

}
