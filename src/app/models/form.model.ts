export const FormDetails = {
    "Login": [
        {
            type: "text",
            label: "Email",
            property: "email"
        },
        {
            type: "password",
            label: "Password",
            property: "password"
        }
    ],
    "Register": [
        {
            type: "text",
            label: "Email",
            property: "email"
        },
        {
            type: "password",
            label: "Password",
            property: "password"
        },
        {
            type: "text",
            label: "Name",
            property: "name"
        },
        {
            type: "text",
            label: "Username",
            property: "username"
        }
    ]
}